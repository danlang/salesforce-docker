# README #

Repo to hold onto resources needed for Docker builds for Salesforce.com Projects

Also includes resources for bundling static resources, allowing individual files within static resources to be commited and then bundled before deploy. Note that the resource bundler is not part of the docker image, its just here so you can download and use it if you want within your repo. Note it requires the additional lib folder within the ant libs folder for zipping the static resources into the resource bundle for salesforce.

